/* 
Copyright (c) 2018, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Arjun Menon
Email id: c.arjunmenon@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package nb_dcache_tb;
  import Vector::*;
  import FIFOF::*;
  import DReg::*;
  import SpecialFIFOs::*;
  import BRAMCore::*;
  import FIFO::*;
  import GetPut::*;
  //import dcache_nway::*;
  import test_caches::*;
  //import icache_dm::*;
  import mem_config::*;
  import BUtils ::*;
  import RegFile::*;
  import Vector::*;
	import nb_dcache_types::*;
	import nb_dcache::*;
	import device_common::*;
	`include "parameters.txt"
  `include "Logger.bsv"           // for logging
  
  `define sets 64
  `define word_size 4
  `define block_size 8
  `define addr_width 32
  `define ways 4
  `define repl RROBIN

  (*synthesize*)
  module mktest(Ifc_test_caches#(`Wordsize , `Linesize , `Setsize , `Ways , `Buswidth, `Paddr));
    let ifc();
    mktest_caches _temp(ifc);
    return (ifc);
  endmodule

  function Bool isIO(Bit#(32) addr, Bool cacheable);
    if(!cacheable)
      return True;
    else if( addr < 4096)
      return True;
    else
      return False;    
  endfunction


  (*synthesize*)
  module mknb_dcache_tb(Empty);

  let dcache <- mkdcache();
  let testcache<- mktest();

  RegFile#(Bit#(10), Bit#(TAdd#(TAdd#(TMul#(`Wordsize, 8), 8), `Paddr ) )) stim <- 
                                                                      mkRegFileFullLoad("test.mem");
  `ifdef pysimulate
  	RegFile#(Bit#(10), Bit#(1))  e_meta <- mkRegFileFullLoad("gold.mem");
	`endif
  RegFile#(Bit#(19), Bit#(`Buswidth)) data <- mkRegFileFullLoad("data.mem");

  Reg#(Bit#(32)) index<- mkReg(0);
  Reg#(Bit#(32)) e_index<- mkReg(0);
  Reg#(Maybe#(Read_req_to_mem#(`Paddr, `Id_bits))) read_mem_req<- mkReg(tagged Invalid);
  Reg#(Maybe#(Write_req_to_mem#(32,TMul#(`Linesize, TMul#(`Wordsize ,8))))) 
                                                            write_mem_req <- mkReg(tagged Invalid);
  Reg#(Bit#(8)) rg_read_burst_count <- mkReg(0);
  Reg#(Bit#(8)) rg_write_burst_count <- mkReg(0);
  Reg#(Bit#(32)) rg_test_count <- mkReg(1);

  FIFOF#(Bit#(TAdd#(TAdd#(TMul#(`Wordsize, 8), 8), `Paddr ) )) ff_req <- mkSizedFIFOF(32);
  `ifdef pysimulate
    FIFOF#(Bit#(1)) ff_meta <- mkSizedFIFOF(32);
  `endif

  `ifdef perf
  Vector#(5,Reg#(Bit#(32))) rg_counters <- replicateM(mkReg(0));
  rule performance_counters;
    Bit#(5) incr = dcache.perf_counters;
    for(Integer i=0;i<5;i=i+1)
      rg_counters[i]<=rg_counters[i]+zeroExtend(incr[i]);
  endrule
  `endif

  //rule enable_disable_cache;
  //  dcache.cache_enable(True);
  //endrule

  rule core_req;
    let stime<-$stime;
    if(stime>=(20)) begin
      let req=stim.sub(truncate(index));
      // read/write : delay/nodelay : Fence/noFence : Null 
      Bit#(8) control = req[`Paddr + 7: `Paddr ];
      Bit#(2) readwrite=control[7:6];
      Bit#(3) size=control[5:3];
      Bit#(1) delay=control[2];
      Bit#(1) fence=control[1];
      Bit#(TAdd#(`Paddr ,  8)) request = truncate(req);
      Bit#(TMul#(`Wordsize, 8)) writedata=truncateLSB(req);

      if(request!=0) begin // // not end of simulation
				if(request!='1 && delay==0) begin
					Origin req_origin= (readwrite=='d1)? Load_buffer: Store_buffer;
					Bit#(`Paddr) p_addr= request[`Paddr-1:0];
					Bit#(`Vaddr) lv_addr= zeroExtend(p_addr);	//TODO change this to `Vaddr
					Req_from_core#(`Vaddr, TMul#(`Wordsize, 8)) temp_req= Req_from_core{ 	addr: lv_addr,
																																								access_size: truncate(size),
																																								payload: writedata,
																																								origin: req_origin };
          dcache.subifc_req_from_core.put(temp_req);
				end
        index<=index+1;
        $display($time,"\tTB: Sending core request: ",fshow(req));
      end
      if((delay==0) || request=='1)begin // if not a fence instruction
        //$display($time,"\tTB: Enquiing request: %h",req);
        ff_req.enq(req);
        `ifdef pysimulate
          ff_meta.enq(e_meta.sub(truncate(index)));
        `endif
      end
    end
  endrule

  rule end_sim;
    Bit#(TAdd#(`Paddr ,  8)) request = truncate(ff_req.first());
    if(request==0)begin
    `ifdef perf
      for(Integer i=0;i<5;i=i+1)
        $display($time,"\tTB: Counter-",countName(i),": %d",rg_counters[i]);
    `endif
      $display($time, "\tTB: All Tests PASSED. Total TestCount: %d", rg_test_count-1);
      $finish(0);
    end
  endrule

  rule checkout_request(ff_req.first[39:0]=='1);
    ff_req.deq;
    `ifdef pysimulate
      ff_meta.deq;
    `endif
    rg_test_count<=rg_test_count+1;
    $display($time,"\tTB: ********** Test:%d PASSED****",rg_test_count);
  endrule


  rule core_resp(ff_req.first[39:0]!='1);
    let resp <- dcache.subifc_resp_to_core.get();
    let req = ff_req.first;
    `ifdef pysimulate  
      let meta <- dcache.meta.get();
      let expected_meta=ff_meta.first();
      ff_meta.deq();
    `endif
    ff_req.deq();
    Bit#(8) control = req[`Paddr + 7: `Paddr ];
    Bit#(2) readwrite=control[7:6];
    Bit#(3) size=control[5:3];
    Bit#(1) delay=control[2];
    Bit#(1) fence=control[1];
    Bit#(TMul#(`Wordsize, 8)) writedata=truncateLSB(req);

    let expected_data<-testcache.memory_operation(truncate(req),readwrite,size,zeroExtend(writedata));
    Bool metafail=False;
    Bool datafail=False;
  
    `ifdef pysimulate
     if(expected_meta!=meta)begin
       $display($time,"\tTB: Meta does not match for Req: %h",req);
       $display($time,"\tTB: Expected Meta: %b Received Meta:%b", expected_meta,meta);
       metafail=True;
     end
    `endif
    if(truncate(expected_data)!=resp.data)begin
        $display($time,"\tTB: Output from cache is wrong for Req: %h",req);
        $display($time,"\tTB: Expected: %h, Received: %h",expected_data,resp.data);
        datafail=True;
    end

    if(metafail||datafail)begin
      $display($time,"\tTB: Test: %d Failed",rg_test_count);
      $finish(0);
    end
    else
      $display($time,"\tTB: Core received correct response: ",fshow(resp)," For req: %h",req);
  endrule

  rule read_mem_request(read_mem_req matches tagged Invalid);
    let req<- dcache.subifc_read_req_to_mem.get;
    read_mem_req<=tagged Valid req;
    $display($time,"\tTB: Memory Read request",fshow(req));
  endrule

  rule read_mem_resp(read_mem_req matches tagged Valid .req);
		let addr= req.addr;
		Bit#(3) size= fromInteger(valueOf(TLog#(`Buswidth)))-3;
		let burst= (req.is_burst?8'd3:8'd0);
    if(rg_read_burst_count == burst) begin
      rg_read_burst_count<=0;
      read_mem_req<=tagged Invalid;
    end
    else begin
      rg_read_burst_count<=rg_read_burst_count+1;
      read_mem_req <= tagged Valid Read_req_to_mem { addr: axi4burst_addrgen(burst,size,2,addr),
																										 id: req.id,
																										 is_burst: req.is_burst }; // parameterize
    end
    let v_wordbits = valueOf(TLog#(`Wordsize));
    Bit#(19) index = truncate(addr>>v_wordbits);
    let dat=data.sub(truncate(index));
		let lv_read_resp=	Read_resp_from_mem {data: dat,
																					id: req.id,
																					last: (rg_read_burst_count==burst) };
		dcache.subifc_read_resp_from_mem.put(lv_read_resp);
    $display($time,"\tTB: Memory Read from index: %d for req: ", index, fshow(lv_read_resp));
  endrule
 
	//rule rl_write_mem_req;
  //  let req<- dcache.subifc_write_req_to_mem.get;
	//	Bit#(3) size= fromInteger(valueOf(TLog#(`Buswidth)))-3;
	//	let burst= (req.burst?8'd3:8'd0);
	//	if(req.is_burst) begin
	//		if(rg_write_burst_count == burst) begin
	//			rg_write_burst_count< 0;
	//		end
	//		else begin
	//			rg_write_burst_count<= rg_write_burst_count + 1;
	//		end
	//	end
  //  write_mem_req <= tagged Valid tuple4(axi4burst_addrgen(burst,zeroExtend(size),2,addr),burst,size,nextdata); // parameterize
	//endrule
  rule write_mem_request(write_mem_req matches tagged Invalid);
    let req<- dcache.subifc_write_req_to_mem.get;
    write_mem_req<=tagged Valid req;
    $display($time,"\tTB: Memory Write request",fshow(req));
  endrule

  rule write_mem_resp(write_mem_req matches tagged Valid .req);
    let addr= req.addr;
		let writedata= req.data;
		Bit#(3) size= fromInteger(valueOf(TLog#(`Buswidth)))-3;
		let burst= (req.is_burst?8'd3:8'd0);
    if(rg_write_burst_count == burst) begin
      rg_write_burst_count<=0;
      write_mem_req<=tagged Invalid;
      dcache.subifc_write_resp_from_mem.put(False);
      $display($time,"\tTB: Sending write response back");
    end
    else begin
      rg_write_burst_count<=rg_write_burst_count+1;
      let nextdata=writedata>>32;
      write_mem_req <= tagged Valid Write_req_to_mem {addr: axi4burst_addrgen(burst,zeroExtend(size),2,addr),
																											is_burst: req.is_burst,
																											data: nextdata };
    end
    
    let v_wordbits = valueOf(TLog#(`Wordsize));
    Bit#(19) index = truncate(addr>>v_wordbits);
    let loaded_data=data.sub(index);

    Bit#(`Buswidth) mask = size[1:0]==0?'hFF:size[1:0]==1?'hFFFF:size[1:0]==2?'hFFFFFFFF:size[1:0]==3?'hFFFFFFFF_FFFF_FFFF:'1;
    Bit#(TLog#(`Wordsize)) shift_amt=addr[v_wordbits-1:0];
    mask= mask<<shift_amt;

    Bit#(`Buswidth) write_word=~mask&loaded_data|mask&truncate(writedata);
    data.upd(index,write_word);
    $display($time,"\tTB: Updating Memory index: %d with: %h burst_count: %d burst: %d", 
      index,write_word,rg_write_burst_count,burst);
  endrule


  rule extra_line;
    $display("\n");
  endrule

endmodule

endpackage

